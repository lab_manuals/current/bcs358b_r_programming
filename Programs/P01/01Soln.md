## Question

**Demonstrate the steps for installation of R and R Studio. Perform the following:**  

    1. Assign different type of values to variables and display the type of variable.  
    Assign different types such as Double, Integer, Logical, Complex and Character and understand the difference between each data type.  
    2. Demonstrate Arithmetic and Logical Operations with simple examples.  
    3. Demonstrate generation of sequences and creation of vectors.  
    4. Demonstrate Creation of Matrices  
    5. Demonstrate the Creation of Matrices from Vectors using Binding Function.  
    6. Demonstrate element extraction from vectors, matrices and arrays  

## Installation of R and R Studio.
### Installing R:  

1. **Download R** : Visit the official R website to download the R distribution for your operating system. The website URL is "https://cran.r-project.org/". Choose the appropriate download link for your operating system (Linux, or Windows).
2. **Install R** :  
    * For Linux: Follow the instructions for your specific Linux distribution. You can often use package managers like *apt* or *yum* to install R.  
    On Ubuntu you can use the command  
    ```console
    $ sudo apt install r-base
    ```
    * For Windows: Double-click the downloaded executable file and follow the installation instructions.

### Installing RStudio:  

1. **Download RStudio** : 
    * Visit the official RStudio website: "https://www.rstudio.com/products/rstudio/download/".
    * Choose the appropriate RStudio Desktop version for your operating system (Linux, or Windows).
2. **Install RStudio:**  
    * For Linux: Download the .deb or .rpm file depending on your Linux distribution and use your package manager to install it. Alternatively, you can use the tarball.
    * For Windows: Double-click the downloaded .exe file and follow the installation instructions.
3. **Launch RStudio:** After installation, launch RStudio by clicking on the RStudio icon or running it from your Applications menu.

You have now successfully installed both R and RStudio on your computer. RStudio is a popular integrated development environment (IDE) that provides a user-friendly interface for working with R, making it easier to write, run, and manage your R scripts and projects. You can start using R by opening RStudio and running R code in its console or script editor.








## Assign different type of values to variables and display the type of variable.
<div align="center">
![a1](img/a1.png "a1")
</div>
<div align="center">
![a2](img/a2.png "a2")
</div>

## Demonstrate Arithmetic and Logical Operations with simple examples.
<div align="center">
![b1](img/b1.png "b1")
</div>
<div align="center">
![b2](img/b2.png "b2")
</div>

## Demonstrate generation of sequences and creation of vectors.
<div align="center">
![c](img/c.png "c")
</div>

## Demonstrate Creation of Matrices
<div align="center">
![d](img/d.png "d")
</div>

## Demonstrate the Creation of Matrices from Vectors using Binding Function.
<div align="center">
![e](img/e.png "e")
</div>

## Demonstrate element extraction from vectors, matrices and arrays
<div align="center">
![f1](img/f1.png "f1")
</div>
<div align="center">
![f2](img/f2.png "f2")
</div>
