---
output:
  html_document: default
  pdf_document: default
editor_options: 
  markdown: 
    wrap: 72
---

# Question

Design a data frame in R for storing about 20 employee details. Create a
CSV file named "input.csv" that defines all the required information
about the employee such as id, name, salary, start_date, dept. Import
into R and do the following analysis. a) Find the total number rows &
columns\
b) Find the maximum salary\
c) Retrieve the details of the employee with maximum salary\
d) Retrieve all the employees working in the IT Department.\
e) Retrieve the employees in the IT Department whose salary is greater
than 20000 and write these details into another file "output.csv"\

# Solution

### Step 1: Create a CSV file named "input.csv" with employee details.

Here's an example of how you can create a data frame in R with 20
employee details and then save it to a CSV file:

```         
# Create a data frame with employee details
employee_data <- data.frame(
  id = 1:20,
  name = c("John", "Alice", "Bob", "Mary", "David", "Sara", "Michael", "Olivia", "Lucas", "Emma", "James", "Sophia", "Ethan", "Ava", "William", "Mia", "Benjamin", "Charlotte", "Henry", "Ella"),
  salary = c(45000, 55000, 60000, 70000, 75000, 62000, 80000, 52000, 58000, 67000, 71000, 59000, 68000, 73000, 54000, 61000, 69000, 72000, 63000, 59000),
  start_date = as.Date(c("2023-01-15", "2022-11-05", "2023-02-20", "2021-08-10", "2022-05-15", "2022-03-30", "2022-12-12", "2022-09-18", "2022-06-23", "2023-03-10", "2021-12-05", "2022-10-02", "2022-04-25", "2022-07-29", "2022-11-28", "2022-05-11", "2022-12-31", "2023-01-05", "2022-08-22", "2022-02-15")),
  dept = c("IT", "HR", "IT", "Finance", "IT", "Sales", "IT", "HR", "Finance", "IT", "Sales", "HR", "IT", "Finance", "Sales", "IT", "Finance", "IT", "Finance", "Sales")
)

# Save the data frame to a CSV file
write.csv(employee_data, "input.csv", row.names = FALSE)
```

### Step 2: Import the data from "input.csv" and perform the analysis.

```         
# Import data from "input.csv"
employee_data <- read.csv("input.csv")

# a) Find the total number of rows and columns
n_rows <- nrow(employee_data)
n_cols <- ncol(employee_data)
cat("Total number of rows:", n_rows, "\n")
cat("Total number of columns:", n_cols, "\n")

# b) Find the maximum salary
max_salary <- max(employee_data$salary)
cat("Maximum salary:", max_salary, "\n")

# c) Retrieve the details of the employee with the maximum salary
employee_with_max_salary <- employee_data[employee_data$salary == max_salary, ]
cat("Details of employee with maximum salary:\n")
print(employee_with_max_salary)

# d) Retrieve all the employees working in the IT Department
it_department_employees <- employee_data[employee_data$dept == "IT", ]
cat("Employees working in the IT Department:\n")
print(it_department_employees)

# e) Retrieve the employees in the IT Department whose salary is greater than 20000
it_department_high_salary <- it_department_employees[it_department_employees$salary > 20000, ]

# Write these details into another file "output.csv"
write.csv(it_department_high_salary, "output.csv", row.names = FALSE)

```

## Output

```{r eval=TRUE, echo=FALSE}
# Import data from "input.csv"
employee_data <- read.csv("input.csv")

# a) Find the total number of rows and columns
n_rows <- nrow(employee_data)
n_cols <- ncol(employee_data)
cat("Total number of rows:", n_rows, "\n")
cat("Total number of columns:", n_cols, "\n")

# b) Find the maximum salary
max_salary <- max(employee_data$salary)
cat("Maximum salary:", max_salary, "\n")

# c) Retrieve the details of the employee with the maximum salary
employee_with_max_salary <- employee_data[employee_data$salary == max_salary, ]
cat("Details of employee with maximum salary:\n")
print(employee_with_max_salary)

# d) Retrieve all the employees working in the IT Department
it_department_employees <- employee_data[employee_data$dept == "IT", ]
cat("Employees working in the IT Department:\n")
print(it_department_employees)

# e) Retrieve the employees in the IT Department whose salary is greater than 20000
it_department_high_salary <- it_department_employees[it_department_employees$salary > 20000, ]

# Write these details into another file "output.csv"
write.csv(it_department_high_salary, "output.csv", row.names = FALSE)

```
